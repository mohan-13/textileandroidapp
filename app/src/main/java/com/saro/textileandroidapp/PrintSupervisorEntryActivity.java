package com.saro.textileandroidapp;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.saro.textileandroidapp.Constants.Keys;

public class PrintSupervisorEntryActivity extends AppCompatActivity {

    Intent entryIntent;
    TextView contentText;
    Toolbar toolbar;
    SharedPreferences myShared;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_print_supervisor_entry);
        myShared = getSharedPreferences(Keys.SHARED_MEMORY, Activity.MODE_PRIVATE);

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        entryIntent = getIntent();
        contentText = (TextView)findViewById(R.id.contentText);

        contentText.setText(
                getResources().getString(R.string.RECEIPT_NO) + ":\t\t" + entryIntent.getStringExtra(Keys.RECEIPT_NO) +"\n\n"+
                        getResources().getString(R.string.EMPLOYEE_ID) + ":\t\t" + entryIntent.getStringExtra(Keys.EMP_ID) + "\n\n" +
                        getResources().getString(R.string.EMPLOYEE_NAME) + ":\t\t" + entryIntent.getStringExtra(Keys.EMP_NAME) + "\n\n" +
                        getResources().getString(R.string.SUPERVISOR_ID) + ":\t\t" + entryIntent.getStringExtra(Keys.SUPERVISOR_ID)+"\n\n" +
                        getResources().getString(R.string.SUPERVISOR_NAME) + ":\t\t" + entryIntent.getStringExtra(Keys.SUPERVISOR_NAME) + "\n\n"+
                        getResources().getString(R.string.DATE) + ":\t\t" + entryIntent.getStringExtra(Keys.DATE) + "\n\n"

        );
        for(int pos = 0; pos<entryIntent.getIntExtra(Keys.QUALITY_COUNT,1); pos++){
            contentText.setText(contentText.getText().toString()+
                    getResources().getString(R.string.QUALITY )+ " " + (pos+1) + " : " + "\t\t" + entryIntent.getStringExtra(Keys.QUALITY+pos)+"\n\n"
            );
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if(entryIntent.getIntExtra(Keys.PRINT_ENTRY_TYPE, 0) == 1) {
                    Intent intent = new Intent(this, SupervisorAddProductionActivity.class);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(this, SupervisorModifyProductionActivity.class);
                    startActivity(intent);
                }
                finish();
        }

        return super.onOptionsItemSelected(item);
    }

    public void printReceipt(View v) {
        String text="<SMALL><CENTER><BOLD>"+getResources().getString(R.string.APP_NAME)+"<BR>" +
                "<CENTER>Weavers Receipt<BR>" +
                "<BR>"+
                "<BOLD>Emp ID :"+entryIntent.getStringExtra(Keys.EMP_ID)+" <BR><BOLD>Receipt No: "+entryIntent.getStringExtra(Keys.RECEIPT_NO)+"<BR>"+
                "<BOLD>Name: "+entryIntent.getStringExtra(Keys.EMP_NAME)+"<BR>"+
                "<BOLD>Date :"+entryIntent.getStringExtra(Keys.DATE)+"<BR><BR>";
        String qualityDatas = "";
        for(int pos = 0; pos<entryIntent.getIntExtra(Keys.QUALITY_COUNT,1); pos++){
            qualityDatas += "<BOLD>"+getResources().getString(R.string.QUALITY)+" "+(pos+1)+": "+entryIntent.getStringExtra(Keys.QUALITY+pos)+"<BR>";
        }
        qualityDatas += "<BR><BR><BOLD>Sup ID :"+entryIntent.getStringExtra(Keys.SUPERVISOR_ID)+" <RIGHT>"+"        <BOLD>Signature"+"<BR><CUT>";
        text += qualityDatas;

        try {
            Intent intent = new Intent("pe.diegoveloper.printing");
            //intent.setAction(android.content.Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.putExtra(android.content.Intent.EXTRA_TEXT, text);
            startActivity(intent);
        }catch (Exception e){
            Log.e("my",e.getMessage());
            Toast.makeText(getApplicationContext(),"Printer not connected properly",Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed() {

        if(entryIntent.getIntExtra(Keys.PRINT_ENTRY_TYPE, 0) == 1) {
            Intent intent = new Intent(this, SupervisorAddProductionActivity.class);
            startActivity(intent);
        } else {
            Intent intent = new Intent(this, SupervisorModifyProductionActivity.class);
            startActivity(intent);
        }
        finish();

    }
}
