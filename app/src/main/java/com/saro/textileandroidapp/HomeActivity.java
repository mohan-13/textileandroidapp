package com.saro.textileandroidapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.saro.textileandroidapp.Adapters.UserListAdapter;
import com.saro.textileandroidapp.Constants.Keys;

import java.util.Locale;

public class HomeActivity extends AppCompatActivity {

    ListView userList;
    String[] userListNames;
    Locale myLocale;
    Resources res;
    DisplayMetrics dm;
    Configuration conf;
    Integer[] userListPics = {R.drawable.supervisor, R.drawable.pinner, R.drawable.quality, R.drawable.report, R.drawable.admin, R.drawable.language};
    Context context;
    Activity activity;
    SharedPreferences myShared;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        myShared = getSharedPreferences(Keys.SHARED_MEMORY, Activity.MODE_PRIVATE);
        context = this;
        activity = this;
        userListNames = getApplicationContext().getResources().getStringArray(R.array.USER_LIST);;
        userList = (ListView) findViewById(R.id.userList);
        UserListAdapter userListAdapter = new UserListAdapter(activity, context, userListNames, userListPics);
        userList.setAdapter(userListAdapter);
        userList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent loginIntent = new Intent(getApplicationContext(), LoginActivity.class);
                switch (position) {
                    case 0:
                        loginIntent.putExtra(Keys.LOGIN, 1);
                        loginIntent.putExtra(Keys.LOGIN_TYPE, getResources().getString(R.string.SUPERVISOR));
                        startActivity(loginIntent);
                        break;
                    case 1:
                        loginIntent.putExtra(Keys.LOGIN, 2);
                        loginIntent.putExtra(Keys.LOGIN_TYPE, getResources().getString(R.string.PINNER));
                        startActivity(loginIntent);
                        break;
                    case 2:
                        loginIntent.putExtra(Keys.LOGIN, 3);
                        loginIntent.putExtra(Keys.LOGIN_TYPE, getResources().getString(R.string.QUALITY_CHECKER));
                        startActivity(loginIntent);
                        break;
                    case 3:
                        loginIntent.putExtra(Keys.LOGIN, 4);
                        loginIntent.putExtra(Keys.LOGIN_TYPE, getResources().getString(R.string.REPORT_GENERATOR));
                        startActivity(loginIntent);
                        break;
//                    case 4:
//                        loginIntent.putExtra(Keys.LOGIN, 5);
//                        loginIntent.putExtra(Keys.LOGIN_TYPE, getResources().getString(R.string.ADMIN));
//                        startActivity(loginIntent);
//                        break;
                    case 4:
                        Intent refresh = new Intent(getApplicationContext(), HomeActivity.class);
                        setLocale();
                        startActivity(refresh);
                        break;
                }
                finish();
            }
        });
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        try {
            // res = getResources();
            newConfig = res.getConfiguration();
            // conf.setLocale(myLocale);
            newConfig.locale = myLocale;
            res.updateConfiguration(newConfig, dm);
            Log.e("my", "comes");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setLocale() {
        myLocale = new Locale(myShared.getString(Keys.LANGUAGE, Keys.TAMIL));
        if(myShared.getString(Keys.LANGUAGE, Keys.TAMIL).equals(Keys.ENGLISH)) {
            SharedPreferences.Editor editor = myShared.edit();
            editor.putString(Keys.LANGUAGE, Keys.TAMIL);
            editor.commit();
        } else {
            SharedPreferences.Editor editor = myShared.edit();
            editor.putString(Keys.LANGUAGE, Keys.ENGLISH);
            editor.commit();
        }
        res = getResources();
        dm = res.getDisplayMetrics();
        conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);

    }
}
