package com.saro.textileandroidapp.Adapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.saro.textileandroidapp.R;

import java.util.ArrayList;

public class QualitySpinnerAdapter extends ArrayAdapter<String> {
    Context context;
    Activity activity;
    ArrayList<String> qualityList;
    TextView spinnerItemText;
    ImageView spinnerItemPic;
    public QualitySpinnerAdapter(@NonNull Context context, Activity activity, ArrayList<String> qualityList) {
        super(context, R.layout.activity_pinner_add_shortage, qualityList);
        this.activity = activity;
        this.context = context;
        this.qualityList = qualityList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View rowView = activity.getLayoutInflater().inflate(R.layout.spinnerlayout,null, true);
        spinnerItemText = (TextView)rowView.findViewById(R.id.spinnerItemText);
        spinnerItemPic = (ImageView)rowView.findViewById(R.id.spinnerItemPic);
        spinnerItemText.setText(qualityList.get(position));
        spinnerItemPic.setImageResource(R.drawable.qualitytype);
        return rowView;
    }
}
