package com.saro.textileandroidapp.Adapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.saro.textileandroidapp.R;

public class UserListAdapter extends ArrayAdapter<String> {
    Activity activity;
    Context context;
    String[] userListItemText;
    Integer[] userListItemPic;
    public UserListAdapter(Activity activity, @NonNull Context context, String[] userListItemText, Integer[] userListItemPic) {
        super(context, R.layout.activity_home, userListItemText);
        this.activity = activity;
        this.context = context;
        this.userListItemText = userListItemText;
        this.userListItemPic = userListItemPic;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.userlistlayout,null, true);
        TextView userListItemTextView = (TextView)rowView.findViewById(R.id.userListItemText);
        ImageView userListItemPicView = (ImageView)rowView.findViewById(R.id.userListItemPic);
        userListItemTextView.setText(userListItemText[position]);
        userListItemPicView.setImageResource(userListItemPic[position]);
        return rowView;
    }
}
