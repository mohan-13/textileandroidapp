package com.saro.textileandroidapp.Adapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.saro.textileandroidapp.R;

public class MenuListAdapter extends ArrayAdapter<String> {
    Activity activity;
    Context context;
    String[] menuListItemText;
    Integer[] menuListItemPic;
    public MenuListAdapter(Activity activity, @NonNull Context context, String[] menuListItemText, Integer[] menuListItemPic) {
        super(context, R.layout.activity_home, menuListItemText);
        this.activity = activity;
        this.context = context;
        this.menuListItemText = menuListItemText;
        this.menuListItemPic = menuListItemPic;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.menulistlayout,null, true);
        TextView menuListItemTextView = (TextView)rowView.findViewById(R.id.menuListItemText);
        ImageView menuListItemPicView = (ImageView)rowView.findViewById(R.id.menuListItemPic);
        menuListItemTextView.setText(menuListItemText[position]);
        menuListItemPicView.setImageResource(menuListItemPic[position]);
        return rowView;
    }
}
