package com.saro.textileandroidapp.Adapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.saro.textileandroidapp.Constants.IdValues;
import com.saro.textileandroidapp.Constants.Keys;
import com.saro.textileandroidapp.R;

import java.util.ArrayList;

public class ShortageReportAdapter extends ArrayAdapter<String> {
    Context context;
    Activity activity;
    ArrayList<String> sNoList, userIdList, empIdList, dateList, refNoList, shortageList, empNameList;
    ArrayList<ArrayList<String>> allQualityList;
    int qualityCount;

    public ShortageReportAdapter(Context context, Activity activity, ArrayList<String> sNoList, ArrayList<String> userIdList, ArrayList<String> empIdList, ArrayList<String> empNameList, ArrayList<String> dateList, ArrayList<String> refNoList, ArrayList<String> shortageList, ArrayList<ArrayList<String>> allQualityList, int qualityCount) {
        super(context, R.layout.activity_supervisor_production_report, sNoList);
        this.context = context;
        this.activity = activity;
        this.sNoList = sNoList;
        this.userIdList = userIdList;
        this.empIdList = empIdList;
        this.empNameList = empNameList;
        this.dateList = dateList;
        this.refNoList = refNoList;
        this.allQualityList = allQualityList;
        this.qualityCount = qualityCount;
        this.shortageList = shortageList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View rowView = activity.getLayoutInflater().inflate(R.layout.reportsupervisorlayout, null, true);
        RelativeLayout reportContentLayout = (RelativeLayout)rowView.findViewById(R.id.reportContentTitleLayout);
        TextView dateLabel = (TextView)rowView.findViewById(R.id.dateLabel);
        TextView pinnerIdLabel = (TextView)rowView.findViewById(R.id.supervisorIdLabel);
        TextView employeeIdLabel = (TextView)rowView.findViewById(R.id.employeeIdLabel);
        TextView employeeNameLabel = (TextView)rowView.findViewById(R.id.employeeNameLabel);
        TextView refNoLabel = (TextView)rowView.findViewById(R.id.receiptNoLabel);
        TextView sNoLabel = (TextView)rowView.findViewById(R.id.sNoLabel);

        sNoLabel.setTextAppearance(context, R.style.reportContentStyle);
        dateLabel.setTextAppearance(context, R.style.reportContentStyle);
        refNoLabel.setTextAppearance(context, R.style.reportContentStyle);
        pinnerIdLabel.setTextAppearance(context, R.style.reportContentStyle);
        employeeIdLabel.setTextAppearance(context, R.style.reportContentStyle);
        employeeNameLabel.setTextAppearance(context, R.style.reportContentStyle);
        sNoLabel.setText(sNoList.get(position));
        refNoLabel.setText(refNoList.get(position));
        pinnerIdLabel.setText(userIdList.get(position));
        employeeIdLabel.setText(empIdList.get(position));
        employeeNameLabel.setText(empNameList.get(position));
        dateLabel.setText(dateList.get(position));
        addQualityCountLayout(reportContentLayout, dateLabel, qualityCount, allQualityList.get(position), position);
        return rowView;
    }

    public void addQualityCountLayout(RelativeLayout reportContentTitleLayout, TextView dateLabel, int qualityCount,ArrayList<String> qualityDetails, int position) {
        int pos;
        long qualityTotal = 0;
        for(pos = 0; pos < qualityCount; pos++) {
            RelativeLayout.LayoutParams qualityTextParams = new RelativeLayout.LayoutParams(200, RelativeLayout.LayoutParams.WRAP_CONTENT);
            TextView qualityTextLabel = new TextView(context);
            if(pos==0) {
                qualityTextParams.addRule(RelativeLayout.RIGHT_OF, dateLabel.getId());
            } else {
                qualityTextParams.addRule(RelativeLayout.RIGHT_OF, IdValues.REPORT_TITLE_ID + (pos-1));
            }
            qualityTextParams.setMargins(20,20,0,0);
            qualityTextLabel.setLayoutParams(qualityTextParams);
            qualityTextLabel.setId(IdValues.REPORT_TITLE_ID+pos);
            qualityTextLabel.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            qualityTextLabel.setTextAppearance(context, R.style.reportContentStyle);
            qualityTextLabel.setText(activity.getResources().getString(R.string.QUALITY)+ Keys.SPACE+(pos+1));
            qualityTextLabel.setText(qualityDetails.get(pos));
            qualityTotal += Long.parseLong(qualityDetails.get(pos));
            reportContentTitleLayout.addView(qualityTextLabel);
        }

        RelativeLayout.LayoutParams shortageTextParams = new RelativeLayout.LayoutParams(200, RelativeLayout.LayoutParams.WRAP_CONTENT);
        shortageTextParams.addRule(RelativeLayout.RIGHT_OF, IdValues.REPORT_TITLE_ID + (pos-1));
        TextView shortageTextLabel = new TextView(context);
        shortageTextParams.setMargins(0,20,0,0);
        shortageTextLabel.setLayoutParams(shortageTextParams);
        shortageTextLabel.setId(IdValues.REPORT_TITLE_ID+pos);
        pos++;
        shortageTextLabel.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        shortageTextLabel.setTextAppearance(context, R.style.reportContentStyle);
        shortageTextLabel.setText(activity.getResources().getString(R.string.SHORTAGE));
        shortageTextLabel.setText(shortageList.get(position)+"");
        reportContentTitleLayout.addView(shortageTextLabel);

        RelativeLayout.LayoutParams totalTextParams = new RelativeLayout.LayoutParams(200, RelativeLayout.LayoutParams.WRAP_CONTENT);
        totalTextParams.addRule(RelativeLayout.RIGHT_OF, IdValues.REPORT_TITLE_ID + (pos-1));
        TextView totalTextLabel = new TextView(context);
        totalTextParams.setMargins(0,20,0,0);
        totalTextLabel.setLayoutParams(totalTextParams);
        totalTextLabel.setId(IdValues.REPORT_TITLE_ID+pos);
        totalTextLabel.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        totalTextLabel.setTextAppearance(context, R.style.reportContentStyle);
        totalTextLabel.setText(activity.getResources().getString(R.string.TOTAL));
        qualityTotal += Long.parseLong(shortageList.get(position));
        totalTextLabel.setText(qualityTotal+"");
        reportContentTitleLayout.addView(totalTextLabel);
    }

}
