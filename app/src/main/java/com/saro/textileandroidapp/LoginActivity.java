package com.saro.textileandroidapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.saro.textileandroidapp.Constants.Keys;
import com.saro.textileandroidapp.Constants.Url;
import com.saro.textileandroidapp.Validation.ValidationClass;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    Toolbar toolbar;
    int type;
    String userId, password;
    EditText userIdEditText, passwordEditText;
    Button loginButton;
    RequestQueue requestQueue;
    ProgressDialog progressBar;
    ValidationClass validationClass;
    StringRequest stringRequest;
    SharedPreferences myShared;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        myShared = getSharedPreferences(Keys.SHARED_MEMORY, Activity.MODE_PRIVATE);
        Intent intent = getIntent();
        type= intent.getIntExtra(Keys.LOGIN, 1);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(intent.getStringExtra(Keys.LOGIN_TYPE));
        userIdEditText = (EditText)findViewById(R.id.userId);
        passwordEditText = (EditText)findViewById(R.id.password);
        loginButton = (Button)findViewById(R.id.login);

        //Volley
        requestQueue = Volley.newRequestQueue(getApplicationContext());

        //progress Dialog
        progressBar = new ProgressDialog(this);
        progressBar.setMessage(getApplicationContext().getResources().getString(R.string.PLEASE_WAIT));
        progressBar.setTitle(getApplicationContext().getResources().getString(R.string.LOADING));
        progressBar.setCancelable(false);
        progressBar.setIndeterminate(false);

        validationClass = new ValidationClass();
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!validationClass.validateUserId(userIdEditText.getText().toString())){
                    Toast.makeText(getApplicationContext(),R.string.ENTER_USER_ID, Toast.LENGTH_LONG).show();
                }
                else if(!validationClass.validatePassword(passwordEditText.getText().toString())){
                    Toast.makeText(getApplicationContext(),R.string.ENTER_PASSWORD,Toast.LENGTH_LONG).show();
                }else{
                    progressBar.show();
                    stringRequest = new StringRequest(Request.Method.POST, Url.URL + Url.LOGIN, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.e("my", response);

                            try {
                                JSONObject result = new JSONObject(response);

                                if(result.get(Keys.RESULT).toString().equals(Keys.SUCCESS)){
                                    int userType = Integer.parseInt(result.get(Keys.USER_TYPE).toString());

                                    //Updating datas to DB
                                    SharedPreferences.Editor editor = myShared.edit();
                                    for(int pos = 1; pos <= Integer.parseInt(result.get(Keys.QUALITY_COUNT).toString()); pos++) {
                                        editor.putString(Keys.QUALITY_NAME+pos, result.get(Keys.QUALITY_NAME+pos).toString());
                                    }

                                    editor.putString(Keys.USER_ID,result.get(Keys.USER_ID).toString());
                                    editor.putString(Keys.USER_NAME,result.get(Keys.USER_NAME).toString());
                                    editor.putInt(Keys.QUALITY_COUNT,Integer.parseInt(result.get(Keys.QUALITY_COUNT).toString()));
                                    editor.putInt(Keys.USER_TYPE,Integer.parseInt(result.get(Keys.USER_TYPE).toString()));
                                    editor.putInt(Keys.LOGIN_STATUS,1);
                                    editor.commit();
                                    Intent intent = null;
                                    if(userType==1){
                                        intent = new Intent(getApplicationContext(),SupervisorHomeActivity.class);
                                    }else if(userType==2){
                                        intent = new Intent(getApplicationContext(),PinnerHomeActivity.class);
                                    }else if(userType==3){
                                        intent = new Intent(getApplicationContext(),QualityCheckerHomeActivity.class);
                                    }else{
                                        intent = new Intent(getApplicationContext(),ReportGeneratorHomeActivity.class);
                                    }
                                    startActivity(intent);
                                    finish();
                                }else{
                                    Toast.makeText(getApplicationContext(),R.string.INCORRECT_CREDENTIALS, Toast.LENGTH_LONG).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            progressBar.dismiss();
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressBar.dismiss();
                        }
                    }){
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            HashMap<String,String> loginData = new HashMap<String,String>();
                            loginData.put(Keys.USER_ID,userIdEditText.getText().toString());
                            loginData.put(Keys.USER_TYPE, type + "");
                            loginData.put(Keys.PASSWORD,passwordEditText.getText().toString());
                            return loginData;
                        }
                    };
                    requestQueue.add(stringRequest);
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                startActivity(intent);
                finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        startActivity(intent);
    }
}
