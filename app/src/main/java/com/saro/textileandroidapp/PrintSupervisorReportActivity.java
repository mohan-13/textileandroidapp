package com.saro.textileandroidapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.saro.textileandroidapp.Constants.Keys;
import com.saro.textileandroidapp.Constants.Url;
import com.saro.textileandroidapp.Validation.ValidationClass;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.Key;
import java.util.HashMap;
import java.util.Map;

public class PrintSupervisorReportActivity extends AppCompatActivity {

    Toolbar toolbar;
    SharedPreferences myShared;
    Intent previousPageIntent;
    RequestQueue reportRequestQueue;
    String fromReceiptNo, toReceiptNo;
    StringBuilder reportData, receiptData;
    JSONObject reportResponse;
    ValidationClass validationClass;
    TextView contentText,noofreceipts,noofreceiptslabel;
    Button printbtn;
    ProgressDialog progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_print_supervisor_report);
        previousPageIntent = getIntent();
        myShared = getSharedPreferences(Keys.SHARED_MEMORY, Activity.MODE_PRIVATE);
        validationClass = new ValidationClass();
        reportRequestQueue = Volley.newRequestQueue(this);
        reportData = new StringBuilder();
        receiptData = new StringBuilder();

        //progress Dialog
        progressBar = new ProgressDialog(this);
        progressBar.setMessage(getApplicationContext().getResources().getString(R.string.PLEASE_WAIT));
        progressBar.setCancelable(false);
        progressBar.setIndeterminate(false);

        //Toolbar Options
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.PRINT_SUPERVISOR_REPORT));

        //UI ID Initialisations
        final EditText fromReceipt = findViewById(R.id.fromReceipt);
        final EditText toReceipt = findViewById(R.id.toReceipt);
        final Button searchbtn = findViewById(R.id.reportSrchBtn);
        printbtn = findViewById(R.id.printbutton);
        contentText = findViewById(R.id.contentText);
        noofreceipts=findViewById(R.id.receipt_count);
        noofreceiptslabel=findViewById(R.id.receiptlabel);
        printbtn.setVisibility(View.INVISIBLE);
        noofreceiptslabel.setVisibility(View.INVISIBLE);
        noofreceipts.setVisibility(View.INVISIBLE);

        searchbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fromReceiptNo = fromReceipt.getText().toString();
                toReceiptNo = toReceipt.getText().toString();
                reportData.delete(0,reportData.length());
                if (validationClass.validateReceiptNo(fromReceiptNo) && validationClass.validateReceiptNo(toReceiptNo)) {
                    progressBar.show();
                    fromReceipt.clearFocus();
                    toReceipt.clearFocus();
                    getReport();
                } else {
                    Toast.makeText(PrintSupervisorReportActivity.this, "Invalid Receipt No", Toast.LENGTH_SHORT).show();
                }
            }
        });

        printbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (reportData.toString().isEmpty() || reportResponse == null) {
                    Toast.makeText(PrintSupervisorReportActivity.this, "No Data to Print", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        Intent intent = new Intent("pe.diegoveloper.printing");
                        //intent.setAction(android.content.Intent.ACTION_SEND);
                        intent.setType("text/plain");
                        intent.putExtra(android.content.Intent.EXTRA_TEXT, receiptData.toString());
                        startActivity(intent);
                    } catch (Exception e) {
                        Log.e("my", e.getMessage());
                        Toast.makeText(getApplicationContext(), "Printer not connected properly", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });


    }

    public void getReport() {
        StringRequest reportRequest = new StringRequest(Request.Method.POST, Url.URL + Url.RECEIPT_REPORT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.e("receipt",response);
                    String qualityData[] = new String[myShared.getInt(Keys.QUALITY_COUNT, 1)];
                    reportResponse = new JSONObject(response);
                    long size = Long.parseLong(reportResponse.get(Keys.SIZE).toString());
                    noofreceipts.setVisibility(View.VISIBLE);
                    noofreceiptslabel.setVisibility(View.VISIBLE);
                    noofreceipts.setText(size+"");
                    for (int pos = 0; pos < size; pos++) {
                        reportData.append("<b>&emsp;&emsp;Shunmugam Textiles</b><p></p>");
                        reportData.append("<b>Receipt No:</b>" + reportResponse.get(Keys.RECEIPT_NO + pos).toString() + "\t\t");
                        reportData.append("Date:" + reportResponse.get(Keys.DATE + pos).toString() + "<p></p>");
                        reportData.append("Supervisor ID:" + reportResponse.get(Keys.USER_ID + pos).toString() + "<p></p>");
                        reportData.append("Supervisor Name:" + reportResponse.get(Keys.USER_NAME + pos).toString() + "<p></p>");
                        reportData.append("Emp. ID:" + reportResponse.get(Keys.EMP_ID + pos).toString() + "<p></p>");
                        reportData.append("Emp. Name:" + reportResponse.get(Keys.EMP_NAME + pos).toString() + "<p></p>");
                        for (int qualityCount = 0; qualityCount < myShared.getInt(Keys.QUALITY_COUNT, 1); qualityCount++) {
                            reportData.append(myShared.getString(Keys.QUALITY_NAME + (qualityCount + 1), "") + " :  ");
                            reportData.append(reportResponse.get(Keys.QUALITY + pos + "" + (qualityCount + 1)).toString() + "<p></p>");
                            qualityData[qualityCount] = reportResponse.get(Keys.QUALITY + pos + "" + (qualityCount + 1)).toString();
                        }
                        reportData.append("------------------------------------------------<p></p>");
                        receiptData.append(getFormattedReceipt(reportResponse.get(Keys.EMP_ID + pos).toString(), reportResponse.get(Keys.EMP_NAME + pos).toString(), reportResponse.get(Keys.RECEIPT_NO + pos).toString(), reportResponse.get(Keys.DATE + pos).toString(), qualityData,reportResponse.get(Keys.USER_ID + pos).toString(),reportResponse.get(Keys.USER_NAME + pos).toString() ));
                    }
                    reportData.append("\nUser Name : "+myShared.getString(Keys.USER_NAME, null)+"\t\t\tSignature");
                    receiptData .append( "<BR><BR><BOLD>User Name :"+myShared.getString(Keys.USER_NAME, null)+" <RIGHT>"+"           <BOLD>Signature"+"<BR><CUT>");
                    if(size>0)
                    {
                        printbtn.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        printbtn.setVisibility(View.INVISIBLE);
                        Toast.makeText(PrintSupervisorReportActivity.this, "No Receipt Deatils Found...", Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                contentText.setText(Html.fromHtml(reportData.toString()));
                progressBar.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.NETWORK_ERROR), Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> reportDatas = new HashMap<String, String>();
                reportDatas.put(Keys.USER_ID, myShared.getString(Keys.USER_ID, null));
                reportDatas.put(Keys.USER_SPECIFIC_REPORT, previousPageIntent.getIntExtra(Keys.USER_SPECIFIC_REPORT, 1) + "");
                reportDatas.put(Keys.QUALITY_COUNT, myShared.getInt(Keys.QUALITY_COUNT, 0) + "");
                reportDatas.put(Keys.FROM_RECEIPT, fromReceiptNo);
                reportDatas.put(Keys.TO_RECEIPT, toReceiptNo);
                return reportDatas;
            }
        };
        reportRequestQueue.add(reportRequest);
    }

    public String getFormattedReceipt(String empId, String empName, String receiptNo, String date, String[] qualityData,String sup_id,String sup_name) {
        String receipt = "<SMALL><CENTER><BOLD>" + getResources().getString(R.string.APP_NAME) + "<BR>" +
                "<BR>" +
                "<BOLD>Receipt No : " + receiptNo + "<BR>" +
                "<BOLD>Supervisor ID : "+sup_id+"<BR>"+
                "<BOLD>Supervisor Name : "+sup_name+"<BR>"+
                "<BOLD>Emp ID : " + empId + " <BR>" +
                "<BOLD>Emp Name : " + empName + "<BR>" +
                "<BOLD>Date : " + date + "<BR><BR>";
        String qualityDatas = "";
        for (int pos = 0; pos < myShared.getInt(Keys.QUALITY_COUNT, 1); pos++) {
            qualityDatas += "<BOLD>" + myShared.getString(Keys.QUALITY_NAME + (pos + 1), "") + " : " + qualityData[pos] + "<BR>";
        }

        receipt += qualityDatas;
        receipt += "<BR><BOLD><CENTER>-----------------------------------------------<BR><CUT>";
        return receipt;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
