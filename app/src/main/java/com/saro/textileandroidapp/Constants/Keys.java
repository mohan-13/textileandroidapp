package com.saro.textileandroidapp.Constants;

public class Keys {
    public static final String LANGUAGE = "language";
    public static final String ENGLISH = "en";
    public static final String TAMIL = "tl";

    public static final String LOGIN = "login";
    public static final String LOGIN_TYPE = "loginType";

    public final static String USER_ID = "userId";
    public final static String USER_NAME = "userName";

    public final static String SUPERVISOR_ID = "supervisorId";
    public final static String SUPERVISOR_NAME = "supervisorName";

    public final static String PINNER_ID = "pinnerId";
    public final static String PINNER_NAME = "pinnerName";

    public final static String QUALITY_CHECKER_ID = "qualityCheckerId";
    public final static String QUALITY_CHECKER_NAME = "qualityCheckerName";

    public final static String REPORT_GENERATOR_ID = "reportGeneratorId";
    public final static String REPORT_GENERATOR_NAME = "reportGeneratorName";

    public final static String ADMIN_ID = "adminId";
    public final static String ADMIN_NAME = "adminName";

    public final static String PASSWORD = "password";
    public final static String USER_TYPE = "userType";
    public final static String QUALITY_COUNT = "qualityCount";
    public final static String QUALITY_NAME = "qualityName";
    public final static String EMP_ID = "empId";
    public final static String DATE = "date";
    public final static String EMP_NAME = "empName";
    public final static String QUANTITY = "quantity";
    public static final String QUALITY_BREAK_UP = "qualityBreakUp";
    public final static String QUALITY = "quality";
    public final static String QUALITY_TYPE = "qualityType";
    public final static String RECEIPT_NO = "receiptNo";
    public final static String SIZE = "size";
    public final static String SHORTAGE = "shortage";
    public final static String DAMAGE = "damage";
    public final static String ORIGINAL = "original";
    public final static String REF_ID = "refId";
    //
    public final static String RESULT = "result";
    public final static String SUCCESS = "success";
    public final static String FAILED = "failed";

    public final static String SPACE = " ";
    public final static String COLON = ":";

    //Shared preferences
    public static final String SHARED_MEMORY = "textile";
    public static final String LOGIN_STATUS = "loginStatus";
    public static final String FROM_DATE = "fromDate";
    public static final String TO_DATE = "toDate";
    public static final String FROM_RECEIPT = "fromReceipt";
    public static final String TO_RECEIPT = "toReceipt";

    public static final String USER_SPECIFIC_REPORT = "userSpecificReport";

    public static final String PRINT_ENTRY_TYPE = "printEntryType";
}
