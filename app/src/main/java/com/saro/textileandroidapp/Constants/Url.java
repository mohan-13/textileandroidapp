package com.saro.textileandroidapp.Constants;

public class Url {
    //app url
    //public final static String URL = "http://shunmugamtextile.com/TextileApp/";
    //public final static String URL = "http://192.168.43.181/TextileApp/";
    public final static String URL = "http://192.168.43.26/TextileApp/";

    //login
    public final static String LOGIN = "login.php";

    //Employee
    public final static String GET_EMP_NAME = "Employee/getEmpName.php";

    //Quality
    public final static String ADD_DAMAGE = "Quality/addDamage.php";
    public final static String GET_DAMAGE = "Quality/getDamage.php";
    public final static String UPDATE_DAMAGE = "Quality/updateDamage.php";

    //Pinner
    public final static String ADD_SHORTAGE = "Pinner/addShortage.php";
    public final static String GET_SHORTAGE = "Pinner/getShortage.php";
    public final static String UPDATE_SHORTAGE = "Pinner/updateShortage.php";

    //Supervisor
    public final static String ADD_PRODUCTION = "Supervisor/addProduction.php";
    public final static String GET_PRODUCTION = "Supervisor/getProduction.php";
    public final static String UPDATE_PRODUCTION = "Supervisor/updateProduction.php";

    //ReportGenerator
    public final static String DAMAGE_REPORT = "Report/DamageReport.php";
    public final static String PRODUCTION_REPORT = "Report/productionReport.php";
    public final static String SHORTAGE_REPORT = "Report/ShortageReport.php";
    public final static String RECEIPT_REPORT = "Report/receiptReport.php";


}
