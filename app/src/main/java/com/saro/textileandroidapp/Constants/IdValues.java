package com.saro.textileandroidapp.Constants;

public class IdValues {
    public final static int SUPERVISOR_QUALITY_TEXT_LABEL_ID = 300;
    public final static int SUPERVISOR_QUALITY_TOTAL_TEXT_ID = 100;
    public final static int SUPERVISOR_QUALITY_EDITTEXT_ID = 1;
    public final static int SUPERVISOR_QUALITY_PLUS_BUTTON_ID = 200;
    public final static int REPORT_TITLE_ID = 300;
}
