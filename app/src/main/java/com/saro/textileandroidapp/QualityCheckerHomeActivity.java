package com.saro.textileandroidapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.saro.textileandroidapp.Adapters.MenuListAdapter;
import com.saro.textileandroidapp.Constants.Keys;

public class QualityCheckerHomeActivity extends AppCompatActivity {

    String[] menuListItemText;
    Integer[] menuListItemPic = {R.drawable.addproduction, R.drawable.modifyproduction, R.drawable.reportproduction, R.drawable.logout};
    ListView menuList;
    Context context;
    SharedPreferences myShared;
    Activity activity;
    Toolbar toolbar;

    Intent changePageIntent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quality_checker_home);
        myShared = getSharedPreferences(Keys.SHARED_MEMORY, Activity.MODE_PRIVATE);
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.WELCOME_MSG)+" "+myShared.getString(Keys.USER_NAME, null));
        context = this;
        activity = this;

        menuListItemText = getApplicationContext().getResources().getStringArray(R.array.QUALITY_CHECKER_MENU);

        //setting resource
        menuList = (ListView)findViewById(R.id.menuList);

        MenuListAdapter menuListAdapter = new MenuListAdapter(activity, context, menuListItemText, menuListItemPic);
        menuList.setAdapter(menuListAdapter);

        menuList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                switch (i){
                    case 0:
                        changePageIntent = new Intent(getApplicationContext(),QualityCheckerAddDamageActivity.class);
                        changePageIntent.putExtra(Keys.USER_ID, myShared.getString(Keys.USER_ID,null));
                        changePageIntent.putExtra(Keys.USER_NAME, myShared.getString(Keys.USER_NAME, null));
                        changePageIntent.putExtra(Keys.QUALITY_COUNT,myShared.getInt(Keys.QUALITY_COUNT, 1)+"");
                        startActivity(changePageIntent);
                        break;
                    case 1:
                        changePageIntent = new Intent(getApplicationContext(),QualityCheckerModifyDamageActivity.class);
                        changePageIntent.putExtra(Keys.USER_ID,myShared.getString(Keys.USER_ID, null));
                        changePageIntent.putExtra(Keys.USER_NAME,myShared.getString(Keys.USER_NAME, null));
                        changePageIntent.putExtra(Keys.QUALITY_COUNT,myShared.getInt(Keys.QUALITY_COUNT, 1)+"");
                        startActivity(changePageIntent);
                        break;
                    case 2:
                        changePageIntent = new Intent(getApplicationContext(),QualityCheckerReportActivity.class);
                        changePageIntent.putExtra(Keys.USER_ID,myShared.getString(Keys.USER_ID, null));
                        changePageIntent.putExtra(Keys.USER_NAME,myShared.getString(Keys.USER_NAME, null));
                        changePageIntent.putExtra(Keys.QUALITY_COUNT,myShared.getInt(Keys.QUALITY_COUNT, 1)+"");
                        startActivity(changePageIntent);
                        break;
                    default:
                        SharedPreferences.Editor editor = myShared.edit();
                        editor.clear();
                        editor.commit();
                        changePageIntent = new Intent(getApplicationContext(), HomeActivity.class);
                        startActivity(changePageIntent);
                        finish();
                        break;
                }
            }
        });


    }
}
