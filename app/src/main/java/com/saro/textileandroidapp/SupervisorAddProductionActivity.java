package com.saro.textileandroidapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.saro.textileandroidapp.Constants.IdValues;
import com.saro.textileandroidapp.Constants.Keys;
import com.saro.textileandroidapp.Constants.Url;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class SupervisorAddProductionActivity extends AppCompatActivity {

    //Intent supervisorDetailsIntent;
    String userName, userId;
    int qualityCount;

    TextView supervisorIdTextView;
    TextView dateTextView;
    String selectedDate;

    EditText employeeIdEditText;

    TextView[] qualityTextLabel;
    EditText[] qualityEditText;
    TextView[] qualityTextViewTotal;
    Button[] qualityPlusButton;

    RelativeLayout qualityLayout;

    TextWatcher entryTextWatcher;
    Context context;

    Date date;
    SimpleDateFormat dateFormatObject;

    StringRequest productionEntryRequest;

    RequestQueue entryQueue;

    ProgressDialog progressBar;
    SharedPreferences myShared;
    Toolbar toolbar;

    EditText employeeNameEditText;
    GetEmpNameByEmpIdOnChangeListener getEmpNameByEmpIdOnChangeListener;
    Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_supervisor_add_production);

        myShared = getSharedPreferences(Keys.SHARED_MEMORY, Activity.MODE_PRIVATE);

        context = this;
        activity = this;
        getEmpNameByEmpIdOnChangeListener = new GetEmpNameByEmpIdOnChangeListener(context, activity);
        //toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.ADD_PRODUCTION));

        date = new Date();
        dateFormatObject = new SimpleDateFormat("dd/MM/yyyy");
        entryQueue = Volley.newRequestQueue(this);

        userName = myShared.getString(Keys.USER_NAME, null);

        userId = myShared.getString(Keys.USER_ID, null);

        qualityCount = myShared.getInt(Keys.QUALITY_COUNT, 1);

        supervisorIdTextView = (TextView) findViewById(R.id.supervisorId);

        dateTextView = (TextView) findViewById(R.id.date);
        dateTextView.setText(dateFormatObject.format(date));
        selectedDate = new SimpleDateFormat("yyyy-MM-dd").format(date);

        employeeIdEditText = (EditText) findViewById(R.id.employeeId);
        employeeNameEditText = (EditText) findViewById(R.id.employeeName);
        getEmpNameByEmpIdOnChangeListener.createEmpIdTextChangeListener(employeeIdEditText, employeeNameEditText);

        qualityLayout = (RelativeLayout) findViewById(R.id.qualityLayout);


        //progress Dialog
        progressBar = new ProgressDialog(this);
        progressBar.setMessage(getApplicationContext().getResources().getString(R.string.PLEASE_WAIT));
        progressBar.setTitle(getApplicationContext().getResources().getString(R.string.LOADING));
        progressBar.setCancelable(false);
        progressBar.setIndeterminate(false);

//       set supervisor id;
        supervisorIdTextView.setText(userId);

        qualityTextLabel = new TextView[200];
        qualityEditText = new EditText[200];
        qualityTextViewTotal = new TextView[200];
        qualityPlusButton = new Button[200];

//      creating EditText Dynamically based on the Quality Count
        for (int pos = 0; pos < qualityCount; pos++) {
//        EditText width and Height
            RelativeLayout.LayoutParams qualityTextLabelParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            RelativeLayout.LayoutParams totalTextParams = new RelativeLayout.LayoutParams(150, RelativeLayout.LayoutParams.WRAP_CONTENT);
            RelativeLayout.LayoutParams plusButtonParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

            qualityTextLabel[pos] = new TextView(this);
            qualityEditText[pos] = new EditText(this);
            qualityTextViewTotal[pos] = new TextView(this);
            qualityPlusButton[pos] = new Button(this);

            qualityTextLabel[pos].setText((myShared.getString(Keys.QUALITY_NAME + (pos + 1), Keys.QUALITY + Keys.SPACE + (pos + 1))));
            qualityTextLabel[pos].setTextAppearance(context, R.style.textQualityLableStyle);

            qualityEditText[pos].setSingleLine(true);
            qualityEditText[pos].setInputType(InputType.TYPE_CLASS_NUMBER);
            qualityEditText[pos].setCursorVisible(true);
            // qualityEditText[pos].setBackgroundColor(getResources().getColor(R.color.black));

            qualityEditText[pos].setBackgroundTintList(ContextCompat.getColorStateList(context, R.color.white));
            qualityEditText[pos].setHintTextColor(getResources().getColor(R.color.white));
            qualityEditText[pos].setTextColor(getResources().getColor(R.color.white));

            // qualityTextViewTotal[pos].setPadding(10,10,10,10);
            qualityTextViewTotal[pos].setTextSize(20);
            qualityTextViewTotal[pos].setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
            qualityTextViewTotal[pos].setTextColor(getResources().getColor(R.color.white));

            totalTextParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, 1);

            qualityPlusButton[pos].setText("+");
            qualityPlusButton[pos].setBackgroundColor(getResources().getColor(R.color.black_overlay));
            qualityPlusButton[pos].setTextColor(getResources().getColor(R.color.white));
            qualityPlusButton[pos].setTextSize(20);

            if (pos == 0) {
                qualityTextLabelParams.setMargins(20, 50, 0, 0);
                qualityTextLabel[pos].setLayoutParams(qualityTextLabelParams);
                qualityTextLabel[pos].setId(IdValues.SUPERVISOR_QUALITY_TEXT_LABEL_ID + pos);
                qualityLayout.addView(qualityTextLabel[pos]);

                totalTextParams.setMargins(20, 20, 0, 0);
                totalTextParams.addRule(RelativeLayout.BELOW, qualityTextLabel[pos].getId());
                qualityTextViewTotal[pos].setLayoutParams(totalTextParams);
                qualityTextViewTotal[pos].setId(IdValues.SUPERVISOR_QUALITY_TOTAL_TEXT_ID + pos);
                qualityTextViewTotal[pos].setText("0");
                qualityLayout.addView(qualityTextViewTotal[pos]);

                plusButtonParams.setMargins(20, 20, 20, 0);
                plusButtonParams.addRule(RelativeLayout.BELOW, qualityTextLabel[pos].getId());
                plusButtonParams.addRule(RelativeLayout.LEFT_OF, qualityTextViewTotal[pos].getId());
                qualityPlusButton[pos].setLayoutParams(plusButtonParams);
                qualityPlusButton[pos].setId(IdValues.SUPERVISOR_QUALITY_PLUS_BUTTON_ID + pos);
                qualityLayout.addView(qualityPlusButton[pos]);

                layoutParams.setMargins(0, 20, 20, 0);
                layoutParams.addRule(RelativeLayout.BELOW, qualityTextLabel[pos].getId());
                layoutParams.addRule(RelativeLayout.LEFT_OF, qualityPlusButton[pos].getId());
                qualityEditText[pos].setLayoutParams(layoutParams);
                //  qualityEditText[pos].setHint(getApplicationContext().getResources().getString(R.string.QUALITY)+" "+(pos+1));
                qualityEditText[pos].setId(pos + IdValues.SUPERVISOR_QUALITY_EDITTEXT_ID);
                qualityLayout.addView(qualityEditText[pos]);

            } else {
                qualityTextLabelParams.setMargins(20, 150, 0, 0);
                qualityTextLabelParams.addRule(RelativeLayout.BELOW, qualityTextViewTotal[pos - 1].getId());
                qualityTextLabel[pos].setLayoutParams(qualityTextLabelParams);
                qualityTextLabel[pos].setId(IdValues.SUPERVISOR_QUALITY_TEXT_LABEL_ID + pos);
                qualityLayout.addView(qualityTextLabel[pos]);


                totalTextParams.setMargins(20, 20, 0, 0);
                totalTextParams.addRule(RelativeLayout.BELOW, qualityTextLabel[pos].getId());
                qualityTextViewTotal[pos].setLayoutParams(totalTextParams);
                qualityTextViewTotal[pos].setId(IdValues.SUPERVISOR_QUALITY_TOTAL_TEXT_ID + pos);
                qualityTextViewTotal[pos].setText("0");
                qualityLayout.addView(qualityTextViewTotal[pos]);

                plusButtonParams.setMargins(20, 20, 20, 0);
                plusButtonParams.addRule(RelativeLayout.BELOW, qualityTextLabel[pos].getId());
                plusButtonParams.addRule(RelativeLayout.LEFT_OF, qualityTextViewTotal[pos].getId());
                qualityPlusButton[pos].setLayoutParams(plusButtonParams);
                qualityPlusButton[pos].setId(IdValues.SUPERVISOR_QUALITY_PLUS_BUTTON_ID + pos);
                qualityLayout.addView(qualityPlusButton[pos]);

                layoutParams.addRule(RelativeLayout.BELOW, qualityTextLabel[pos].getId());
                layoutParams.addRule(RelativeLayout.LEFT_OF, qualityPlusButton[pos].getId());
                layoutParams.setMargins(0, 20, 20, 0);
                qualityEditText[pos].setLayoutParams(layoutParams);
                //  qualityEditText[pos].setHint(getApplicationContext().getResources().getString(R.string.QUALITY)+" "+(pos+1));
                qualityEditText[pos].setId(pos + IdValues.SUPERVISOR_QUALITY_EDITTEXT_ID);
                qualityLayout.addView(qualityEditText[pos]);
            }
            addTotalQuality(pos);
            addPlusToQuality(pos);

        }
    }

    public void addTotalQuality(final int position) {

        TextWatcher qualityInputWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String qualityInput = editable.toString();
                int qualityInputLength = qualityInput.length();
//                Toast.makeText(getApplicationContext(),qualityInputLength+"",Toast.LENGTH_LONG).show();
                long qualityTotal = 0;
                if (qualityInputLength != 0) {
                    for (String quality : qualityInput.split("\\+")) {
                        qualityTotal += Long.parseLong(quality);
                    }
                }
                qualityTextViewTotal[position].setText(qualityTotal + "");


            }
        };

        qualityEditText[position].addTextChangedListener(qualityInputWatcher);
    }

    public void addPlusToQuality(final int position) {

        qualityPlusButton[position].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (qualityEditText[position].getText().toString().length() != 0) {
                    String qualityLastValue = qualityEditText[position].getText().toString();
                    qualityLastValue = qualityLastValue.substring(qualityLastValue.length() - 1);
                    if (qualityLastValue.equals("+")) {

                    } else {
                        qualityEditText[position].setText(qualityEditText[position].getText().toString() + "+");
                        qualityEditText[position].setSelection(qualityEditText[position].getText().toString().length());
                    }
                }

            }
        });
    }

    public void changeDate(View v) {

        final AlertDialog.Builder calendarAlertBuilder = new AlertDialog.Builder(this);
        calendarAlertBuilder.setCancelable(false);

        LayoutInflater inflater = LayoutInflater.from(this);
        final View calendarView = inflater.inflate(R.layout.calendar, null);

        final DatePicker datePicker = (DatePicker) calendarView.findViewById(R.id.calendar);
        Button okButton = (Button) calendarView.findViewById(R.id.ok);

        calendarAlertBuilder.setView(calendarView);

        final AlertDialog calendarAlert = calendarAlertBuilder.create();
        calendarAlert.show();

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dateTextView.setText(datePicker.getDayOfMonth() + "/" + (datePicker.getMonth() + 1) + "/" + datePicker.getYear());
                selectedDate = datePicker.getYear() + "-" + (datePicker.getMonth() + 1) + "-" + datePicker.getDayOfMonth();
                calendarAlert.dismiss();
            }
        });
    }

    public void saveProduction(View v) {
        if (employeeIdEditText.getText().toString().trim().equals("") || employeeIdEditText.getText().toString() == null) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.EMPLOYEE_ID_MUST), Toast.LENGTH_SHORT).show();
        } else {
            progressBar.show();
            productionEntryRequest = new StringRequest(Request.Method.POST, Url.URL + Url.ADD_PRODUCTION, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    progressBar.dismiss();
                    Log.e("my", response);
                    try {

                        JSONObject responseData = new JSONObject(response);
                        if (responseData.get(Keys.RESULT).toString().equals(Keys.SUCCESS)) {
                            Intent printIntent = new Intent(getApplicationContext(), PrintSupervisorEntryActivity.class);
                            printIntent.putExtra(Keys.SUPERVISOR_ID, userId);
                            printIntent.putExtra(Keys.SUPERVISOR_NAME, userName);
                            printIntent.putExtra(Keys.QUALITY_COUNT, qualityCount);
                            printIntent.putExtra(Keys.EMP_ID, responseData.get(Keys.EMP_ID).toString());
                            printIntent.putExtra(Keys.EMP_NAME, responseData.get(Keys.EMP_NAME).toString());
                            printIntent.putExtra(Keys.RECEIPT_NO, responseData.get(Keys.RECEIPT_NO).toString());
                            printIntent.putExtra(Keys.DATE, responseData.get(Keys.DATE).toString());
                            printIntent.putExtra(Keys.PRINT_ENTRY_TYPE, 1);
                            for (int pos = 0; pos < qualityCount; pos++) {
                                printIntent.putExtra(Keys.QUALITY + pos, qualityTextViewTotal[pos].getText().toString());
                            }
                            startActivity(printIntent);
                            finish();
                        } else {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.EMPLOYEE_ID_MUST), Toast.LENGTH_LONG).show();
                        }

                    } catch (Exception e) {
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressBar.dismiss();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    HashMap<String, String> productionDataMap = new HashMap<String, String>();
                    productionDataMap.put(Keys.USER_ID, userId);
                    productionDataMap.put(Keys.EMP_ID, employeeIdEditText.getText().toString());
                    productionDataMap.put(Keys.DATE, selectedDate);
                    productionDataMap.put(Keys.QUALITY_COUNT, qualityCount + "");

                    for (int pos = 0; pos < qualityCount; pos++) {
                        productionDataMap.put(Keys.QUALITY_TYPE + pos, (pos + 1) + "");
                        productionDataMap.put(Keys.QUANTITY + pos, qualityTextViewTotal[pos].getText().toString());
                        productionDataMap.put(Keys.QUALITY_BREAK_UP + pos, qualityEditText[pos].getText().toString());
                    }

                    return productionDataMap;
                }
            };

            entryQueue.add(productionEntryRequest);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }

        return super.onOptionsItemSelected(item);
    }


}
