package com.saro.textileandroidapp.Validation;

import android.content.Intent;

public class ValidationClass {

    public boolean validateUserId(String userId){

        if(userId == null || userId.equals("") || userId.equals(" ")){
            return false;
        }
        return true;
    }
    public boolean validatePassword(String passWord){
        if (passWord == null || passWord.equals("") || passWord.equals(" ")){
            return false;
        }
        return true;
    }

    public boolean validateValues(String value) {
        if(value == null || value.equals("") || value.equals(" ")){
            return false;
        }
        return true;
    }
    public boolean validateReceiptNo(String no)
    {
        if(no == null || no.equals("") || no.equals(" ")){
            return false;
        }
        try
        {
            int num= Integer.parseInt(no);
            return true;
        }
        catch (Exception exception)
        {
            return false;
        }

    }

}

