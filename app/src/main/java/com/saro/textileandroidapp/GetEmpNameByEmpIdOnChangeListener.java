package com.saro.textileandroidapp;

import android.app.Activity;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.saro.textileandroidapp.Constants.Url;

import org.json.JSONObject;

public class GetEmpNameByEmpIdOnChangeListener {
    Context context;
    Activity activity;
    RequestQueue queue;

    public GetEmpNameByEmpIdOnChangeListener(Context context, Activity activity) {
        this.context = context;
        this.activity = activity;
        queue = Volley.newRequestQueue(context);
    }

    public void createEmpIdTextChangeListener(final EditText empIdEditText, final EditText empNameEditText) {
        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                getEmpName(empIdEditText.getText().toString(), empNameEditText);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
        empIdEditText.addTextChangedListener(textWatcher);
    }

    private void getEmpName(String empId, final EditText empNameEditText) {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, Url.URL + Url.GET_EMP_NAME + "?emp_id="+empId, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.e("my", response);
                    JSONObject responseJsonObject = new JSONObject(response);
                    empNameEditText.setText(responseJsonObject.get("emp_name").toString());
                } catch (Exception e) {
                    empNameEditText.setText("Unknown Id");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        queue.add(stringRequest);
    }
}
