package com.saro.textileandroidapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.saro.textileandroidapp.Adapters.QualitySpinnerAdapter;
import com.saro.textileandroidapp.Constants.Keys;
import com.saro.textileandroidapp.Constants.Url;
import com.saro.textileandroidapp.Validation.ValidationClass;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class QualityCheckerModifyDamageActivity extends AppCompatActivity {

    String userName, userId;
    int qualityCount;

    TextView qualityCheckerIdTextView;
    TextView dateTextView;
    String selectedDate;
    EditText originalQuantity,damagedQuantity;
    EditText refNoText;
    TextView actualQuantity;

    EditText employeeIdEditText;

    RelativeLayout qualityLayout;

    TextWatcher entryTextWatcher;

    Date date;
    SimpleDateFormat dateFormatObject;

    StringRequest productionEntryRequest, refNoRequest;

    RequestQueue entryQueue;

    ProgressDialog progressBar;
    SharedPreferences myShared;
    Toolbar toolbar;
    Spinner qualityTypeSpinner;
    ArrayList<String> qualityTypeList = new ArrayList<String>();
    Context context;
    Activity activity;
    EditText employeeNameEditText;
    GetEmpNameByEmpIdOnChangeListener getEmpNameByEmpIdOnChangeListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quality_checker_modify_damage);

        myShared = getSharedPreferences(Keys.SHARED_MEMORY, Activity.MODE_PRIVATE);
        context = getApplicationContext();
        activity = this;
        //toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.MODIFY_DAMAGE));

        date = new Date();
        dateFormatObject = new SimpleDateFormat("dd/MM/yyyy");
        entryQueue = Volley.newRequestQueue(this);

        userName = myShared.getString(Keys.USER_NAME, null);
        userId = myShared.getString(Keys.USER_ID, null);
        qualityCount = myShared.getInt(Keys.QUALITY_COUNT, 1);
        qualityCheckerIdTextView = (TextView) findViewById(R.id.pinnerId);

        dateTextView = (TextView) findViewById(R.id.date);
//        dateTextView.setText(dateFormatObject.format(date));
        selectedDate = new SimpleDateFormat("yyyy-MM-dd").format(date);

        employeeIdEditText = (EditText) findViewById(R.id.employeeId);
        employeeNameEditText = (EditText)findViewById(R.id.employeeName);
        getEmpNameByEmpIdOnChangeListener = new GetEmpNameByEmpIdOnChangeListener(context, activity);
        getEmpNameByEmpIdOnChangeListener.createEmpIdTextChangeListener(employeeIdEditText, employeeNameEditText);


        originalQuantity = (EditText) findViewById(R.id.originalCountText);
        damagedQuantity = (EditText) findViewById(R.id.damagedCountText);
        actualQuantity = (TextView) findViewById(R.id.actualCountText);
        refNoText = (EditText) findViewById(R.id.refNoText);

        qualityLayout = (RelativeLayout) findViewById(R.id.qualityLayout);
        qualityTypeSpinner = (Spinner) findViewById(R.id.qualityTypeSpinner);

        addSpinnerQualityTypes(qualityTypeList, qualityCount);
        QualitySpinnerAdapter qualitySpinnerAdapter = new QualitySpinnerAdapter(context, activity, qualityTypeList);
        qualitySpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        qualityTypeSpinner.setAdapter(qualitySpinnerAdapter);
        //progress Dialog
        progressBar = new ProgressDialog(this);
        progressBar.setMessage(getApplicationContext().getResources().getString(R.string.PLEASE_WAIT));
        progressBar.setTitle(getApplicationContext().getResources().getString(R.string.LOADING));
        progressBar.setCancelable(false);
        progressBar.setIndeterminate(false);

//       set supervisor id;
        qualityCheckerIdTextView.setText(userId);

        entryTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                long originalQuantityCount, damagedQuantityCount;
                if (originalQuantity.getText().toString() == null || originalQuantity.getText().toString().equals("")) {
                    originalQuantityCount = 0;
                } else {
                    originalQuantityCount = Long.parseLong(originalQuantity.getText().toString());
                }
                if (damagedQuantity.getText().toString() == null || damagedQuantity.getText().toString().equals("")) {
                    damagedQuantityCount = 0;
                } else {
                    damagedQuantityCount = Long.parseLong(damagedQuantity.getText().toString());
                }
                actualQuantity.setText(originalQuantityCount - damagedQuantityCount + "");
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };

        damagedQuantity.addTextChangedListener(entryTextWatcher);
        originalQuantity.addTextChangedListener(entryTextWatcher);
    }

    public void addSpinnerQualityTypes(ArrayList<String> qualityTypeList, int qualityCount) {
        for(int pos = 0; pos < qualityCount; pos++) {
            qualityTypeList.add(myShared.getString(Keys.QUALITY_NAME+(pos+1),Keys.QUALITY+Keys.SPACE+(pos+1)));
        }
    }

    public void searchRefNo(View v) {
        progressBar.show();
        refNoRequest = new StringRequest(Request.Method.POST, Url.URL + Url.GET_DAMAGE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.e("my",response);
                    JSONObject responseData = new JSONObject(response);
                    int size = Integer.parseInt(responseData.get(Keys.SIZE).toString());
                    if(size == 1 ) {
                        employeeIdEditText.setText(responseData.get(Keys.EMP_ID).toString());
                        qualityTypeSpinner.setSelection(Integer.parseInt(responseData.get(Keys.QUALITY).toString()) - 1);
                        originalQuantity.setText(responseData.get(Keys.ORIGINAL).toString());
                        actualQuantity.setText(responseData.get(Keys.QUANTITY).toString());
                        damagedQuantity.setText(responseData.get(Keys.DAMAGE).toString());
                    } else {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.INVALID_RECEIPT_NUMBER), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                progressBar.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.NETWORK_ERROR), Toast.LENGTH_SHORT).show();
                progressBar.dismiss();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> receiptNoData = new HashMap<String,String>();
                receiptNoData.put(Keys.REF_ID, refNoText.getText().toString());
                return receiptNoData;
            }
        };
        entryQueue.add(refNoRequest);
    }


    public void updateDamage(View v){
        ValidationClass validationClass = new ValidationClass();
        if(!validationClass.validateValues(employeeIdEditText.getText().toString())){
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.EMPLOYEE_ID_MUST), Toast.LENGTH_SHORT).show();
        }else {
            if (!validationClass.validateValues(refNoText.getText().toString()) || !validationClass.validateValues(originalQuantity.getText().toString()) || !validationClass.validateValues(damagedQuantity.getText().toString())) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.INVALID_ENTRY), Toast.LENGTH_SHORT).show();
            } else {
                progressBar.show();
                productionEntryRequest = new StringRequest(Request.Method.POST, Url.URL + Url.UPDATE_DAMAGE, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("my", response);
                        try {

                            JSONObject responseData = new JSONObject(response);
                            if (responseData.get(Keys.RESULT).toString().equals(Keys.SUCCESS)) {
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(QualityCheckerModifyDamageActivity.this);
                                View alertView = activity.getLayoutInflater().inflate(R.layout.receiptalertboxlayout, null);
                                alertDialogBuilder.setView(alertView);
                                alertDialogBuilder.setCancelable(false);
                                TextView refNoText = (TextView) alertView.findViewById(R.id.refNoText);
                                Button closeBtn = (Button) alertView.findViewById(R.id.closeAlertBox);
                                refNoText.setText(responseData.get(Keys.REF_ID).toString());
                                alertDialogBuilder.setMessage(getResources().getString(R.string.SUCCESSFULLY_UPDATED));
                                final AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();
                                closeBtn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        alertDialog.dismiss();
                                        Intent intent = getIntent();
                                        startActivity(intent);
                                        finish();
                                    }
                                });
                            } else {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.INVALID_ENTRY), Toast.LENGTH_LONG).show();
                            }


                        } catch (Exception e) {
                        }
                        progressBar.dismiss();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressBar.dismiss();
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.NETWORK_ERROR), Toast.LENGTH_LONG).show();
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {

                        HashMap<String, String> shortageData = new HashMap<String, String>();
                        shortageData.put(Keys.USER_ID, userId);
                        shortageData.put(Keys.REF_ID, refNoText.getText().toString());
                        shortageData.put(Keys.EMP_ID, employeeIdEditText.getText().toString());
                        shortageData.put(Keys.QUALITY_TYPE, qualityTypeSpinner.getSelectedItemPosition() + 1 + "");
                        shortageData.put(Keys.QUANTITY, actualQuantity.getText().toString());
                        shortageData.put(Keys.DAMAGE, damagedQuantity.getText().toString());
                        return shortageData;
                    }
                };

                entryQueue.add(productionEntryRequest);
            }
        }
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }

        return super.onOptionsItemSelected(item);
    }

}
