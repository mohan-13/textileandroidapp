package com.saro.textileandroidapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.saro.textileandroidapp.Adapters.ProductionReportAdapter;
import com.saro.textileandroidapp.Adapters.ShortageReportAdapter;
import com.saro.textileandroidapp.Constants.IdValues;
import com.saro.textileandroidapp.Constants.Keys;
import com.saro.textileandroidapp.Constants.Url;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class PinnerReportActivity extends AppCompatActivity {

    ListView menuList;
    Context context;
    SharedPreferences myShared;
    Activity activity;
    Toolbar toolbar;
    RelativeLayout reportContentTitleLayout;
    TextView dateLabel;
    TextView fromDateLabel, toDateLabel, refIdLabel, pinnerIdLabel;
    int qualityCount;
    String fromDate, toDate;
    RequestQueue reportRequestQueue;
    Date date;
    SimpleDateFormat dateFormatObject;
    ListView reportContentList;
    ProgressDialog progressBar;
    Intent previousPageIntent;
    RelativeLayout bottomLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pinner_report);
        myShared = getSharedPreferences(Keys.SHARED_MEMORY, Activity.MODE_PRIVATE);
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.PINNER_REPORT));
        context = this;
        activity = this;
        previousPageIntent = getIntent();

        date = new Date();
        dateFormatObject = new SimpleDateFormat("dd/MM/yyyy");
        fromDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
        toDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
        fromDateLabel = (TextView)findViewById(R.id.fromDate);
        toDateLabel = (TextView)findViewById(R.id.toDate);
        refIdLabel = (TextView)findViewById(R.id.receiptNoLabel);
        pinnerIdLabel = (TextView)findViewById(R.id.supervisorIdLabel);
        refIdLabel.setText(getResources().getString(R.string.REF_NO));
        pinnerIdLabel.setText(getResources().getString(R.string.PINNER_ID));
        fromDateLabel.setText(fromDate);
        toDateLabel.setText(toDate);

        reportRequestQueue = Volley.newRequestQueue(context);
        reportContentTitleLayout = (RelativeLayout)findViewById(R.id.reportContentTitleLayout);
        dateLabel = (TextView)findViewById(R.id.dateLabel);
        reportContentList = (ListView)findViewById(R.id.reportContentList);

        qualityCount = myShared.getInt(Keys.QUALITY_COUNT, 1);
        TextView dateLabelBottom = (TextView)findViewById(R.id.dateLabelBottom);
        bottomLayout = (RelativeLayout)findViewById(R.id.bottomTotalLayout);
        addQualityCountToTitleLayout(reportContentTitleLayout, dateLabel, qualityCount);
        addBottomTotalLayout(bottomLayout, dateLabelBottom, qualityCount);

        //progress Dialog
        progressBar = new ProgressDialog(this);
        progressBar.setMessage(getApplicationContext().getResources().getString(R.string.PLEASE_WAIT));
        progressBar.setTitle(getApplicationContext().getResources().getString(R.string.LOADING));
        progressBar.setCancelable(false);
        progressBar.setIndeterminate(false);

        getReport();
    }

    public void addQualityCountToTitleLayout(RelativeLayout reportContentTitleLayout, TextView dateLabel, int qualityCount) {
        int pos;
        for(pos = 0; pos < qualityCount; pos++) {
            RelativeLayout.LayoutParams qualityTextParams = new RelativeLayout.LayoutParams(200, RelativeLayout.LayoutParams.WRAP_CONTENT);
            TextView qualityTextLabel = new TextView(context);
            if(pos==0) {
                qualityTextParams.addRule(RelativeLayout.RIGHT_OF, dateLabel.getId());
            } else {
                qualityTextParams.addRule(RelativeLayout.RIGHT_OF, IdValues.REPORT_TITLE_ID + (pos-1));
            }
            qualityTextParams.setMargins(20,20,0,0);
            qualityTextLabel.setLayoutParams(qualityTextParams);
            qualityTextLabel.setId(IdValues.REPORT_TITLE_ID+pos);
            qualityTextLabel.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            qualityTextLabel.setTextAppearance(context, R.style.reportTitleStyle);
            qualityTextLabel.setText((myShared.getString(Keys.QUALITY_NAME+(pos+1),Keys.QUALITY+Keys.SPACE+(pos+1))));
            reportContentTitleLayout.addView(qualityTextLabel);
        }

        RelativeLayout.LayoutParams shortageTextParams = new RelativeLayout.LayoutParams(200, RelativeLayout.LayoutParams.WRAP_CONTENT);
        shortageTextParams.addRule(RelativeLayout.RIGHT_OF, IdValues.REPORT_TITLE_ID + (pos-1));

        TextView shortageTextLabel = new TextView(context);
        shortageTextParams.setMargins(0,20,0,0);
        shortageTextLabel.setLayoutParams(shortageTextParams);
        shortageTextLabel.setId(IdValues.REPORT_TITLE_ID+pos);
        pos++;
        shortageTextLabel.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        shortageTextLabel.setTextAppearance(context, R.style.reportTitleStyle);
        shortageTextLabel.setText(getResources().getString(R.string.SHORTAGE));
        reportContentTitleLayout.addView(shortageTextLabel);

        RelativeLayout.LayoutParams totalTextParams = new RelativeLayout.LayoutParams(200, RelativeLayout.LayoutParams.WRAP_CONTENT);
        totalTextParams.addRule(RelativeLayout.RIGHT_OF, IdValues.REPORT_TITLE_ID + (pos-1));

        TextView totalTextLabel = new TextView(context);
        totalTextParams.setMargins(0,20,0,0);
        totalTextLabel.setLayoutParams(totalTextParams);
        totalTextLabel.setId(IdValues.REPORT_TITLE_ID+pos);
        totalTextLabel.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        totalTextLabel.setTextAppearance(context, R.style.reportTitleStyle);
        totalTextLabel.setText(getResources().getString(R.string.TOTAL));
        reportContentTitleLayout.addView(totalTextLabel);
    }
    public void getReport() {
        progressBar.show();
        StringRequest reportRequest = new StringRequest(Request.Method.POST, Url.URL + Url.SHORTAGE_REPORT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("my", response);
                try {
                    JSONObject reportResponse = new JSONObject(response);
                    ArrayList<String> sNoList, userIdList, empIdList, dateList, refNoList, shortageList, empNameList;
                    sNoList = new ArrayList<String>();
                    userIdList = new ArrayList<String>();
                    empIdList = new ArrayList<String>();
                    dateList = new ArrayList<String>();
                    refNoList = new ArrayList<String>();
                    shortageList = new ArrayList<String>();
                    empNameList = new ArrayList<String>();
                    ArrayList<ArrayList<String>> allQualityList = new ArrayList<ArrayList<String>>();
                    long size = Long.parseLong(reportResponse.get(Keys.SIZE).toString());
                    for(long pos = 0; pos < size; pos++) {
                        sNoList.add((pos+1)+"");
                        userIdList.add(reportResponse.get(Keys.USER_ID + pos).toString());
                        empIdList.add(reportResponse.get(Keys.EMP_ID + pos).toString());
                        dateList.add(reportResponse.get(Keys.DATE + pos).toString());
                        refNoList.add(reportResponse.get(Keys.REF_ID + pos).toString());
                        shortageList.add(reportResponse.get(Keys.SHORTAGE + pos).toString());
                        empNameList.add(reportResponse.get(Keys.EMP_NAME + pos).toString());
                        ArrayList<String> qualityList = new ArrayList<String>();
                        for(long qualityCount = 0; qualityCount < myShared.getInt(Keys.QUALITY_COUNT, 1); qualityCount++ ) {
                            qualityList.add(reportResponse.get(Keys.QUALITY+pos+""+(qualityCount+1)).toString());
                        }
                        allQualityList.add(qualityList);
                    }
                    ShortageReportAdapter shortageReportAdapter = new ShortageReportAdapter(context, activity, sNoList, userIdList, empIdList, empNameList, dateList, refNoList, shortageList, allQualityList, qualityCount);
                    reportContentList.setAdapter(shortageReportAdapter);
                    bottomTotalLayout(allQualityList, shortageList,qualityCount);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressBar.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.dismiss();
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.NETWORK_ERROR), Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> reportDatas = new HashMap<String, String>();
                reportDatas.put(Keys.USER_ID, myShared.getString(Keys.USER_ID, null));
                reportDatas.put(Keys.USER_SPECIFIC_REPORT, previousPageIntent.getIntExtra(Keys.USER_SPECIFIC_REPORT, 1)+"");
                reportDatas.put(Keys.QUALITY_COUNT, myShared.getInt(Keys.QUALITY_COUNT, 0) +"");
                reportDatas.put(Keys.FROM_DATE, fromDate);
                reportDatas.put(Keys.TO_DATE, toDate);
                return reportDatas;
            }
        };
        reportRequestQueue.add(reportRequest);
    }

    public void fromDateChange(View v){

        final TextView dateLabel = (TextView) v;
        final AlertDialog.Builder calendarAlertBuilder = new AlertDialog.Builder(this);
        calendarAlertBuilder.setCancelable(false);

        LayoutInflater inflater = LayoutInflater.from(this);
        final View calendarView = inflater.inflate(R.layout.calendar,null);

        final DatePicker datePicker = (DatePicker)calendarView.findViewById(R.id.calendar);
        Button okButton = (Button)calendarView.findViewById(R.id.ok);

        calendarAlertBuilder.setView(calendarView);

        final AlertDialog calendarAlert =  calendarAlertBuilder.create();
        calendarAlert.show();

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // dateLabel.setText(datePicker.getDayOfMonth()+"/"+(datePicker.getMonth()+1)+"/"+datePicker.getYear());
                dateLabel.setText(datePicker.getYear()+"-"+(datePicker.getMonth()+1)+"-"+datePicker.getDayOfMonth());
                fromDate = datePicker.getYear()+"-"+(datePicker.getMonth()+1)+"-"+datePicker.getDayOfMonth();
                calendarAlert.dismiss();
            }
        });
    }

    public void toDateChange(View v){

        final TextView dateLabel = (TextView) v;
        final AlertDialog.Builder calendarAlertBuilder = new AlertDialog.Builder(this);
        calendarAlertBuilder.setCancelable(false);

        LayoutInflater inflater = LayoutInflater.from(this);
        final View calendarView = inflater.inflate(R.layout.calendar,null);

        final DatePicker datePicker = (DatePicker)calendarView.findViewById(R.id.calendar);
        Button okButton = (Button)calendarView.findViewById(R.id.ok);

        calendarAlertBuilder.setView(calendarView);

        final AlertDialog calendarAlert =  calendarAlertBuilder.create();
        calendarAlert.show();

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // dateLabel.setText(datePicker.getDayOfMonth()+"/"+(datePicker.getMonth()+1)+"/"+datePicker.getYear());
                dateLabel.setText(datePicker.getYear()+"-"+(datePicker.getMonth()+1)+"-"+datePicker.getDayOfMonth());
                toDate = datePicker.getYear()+"-"+(datePicker.getMonth()+1)+"-"+datePicker.getDayOfMonth();
                calendarAlert.dismiss();
            }
        });
    }

    public void searchReportByDate(View v){
        getReport();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }

        return super.onOptionsItemSelected(item);
    }

    public void bottomTotalLayout(ArrayList<ArrayList<String>> allQualityList, ArrayList<String> shortageList, int qualityCount) {
        int pos;
        long qualityTotal = 0;
        long[] qualityIndividualTotal = new long[qualityCount];
        long shortageTotal = 0;
        for (ArrayList<String> qualityDetails: allQualityList) {
            for (pos = 0; pos < qualityCount; pos++) {
                qualityIndividualTotal[pos] += Long.parseLong(qualityDetails.get(pos));
            }
        }
        for (String shortage: shortageList) {
            shortageTotal += Long.parseLong(shortage);
        }
        for(pos = 0; pos < qualityCount; pos++) {
            TextView qualityTextLabel = (TextView)findViewById(IdValues.REPORT_TITLE_ID + pos + 150);
            qualityTextLabel.setText(qualityIndividualTotal[pos]+"");
            qualityTotal += qualityIndividualTotal[pos];
        }
        TextView shortageTextLabel = (TextView)findViewById(IdValues.REPORT_TITLE_ID + pos + 150);
        shortageTextLabel.setText(shortageTotal+"");

        TextView totalTextLabel = (TextView)findViewById(IdValues.REPORT_TITLE_ID + (pos+1) + 150);
        totalTextLabel.setText(qualityTotal+shortageTotal + "");
    }

    public void addBottomTotalLayout(RelativeLayout reportContentTitleLayout, TextView dateLabel, int qualityCount) {
        int pos;
        for(pos = 0; pos < qualityCount; pos++) {
            RelativeLayout.LayoutParams qualityTextParams = new RelativeLayout.LayoutParams(200, RelativeLayout.LayoutParams.WRAP_CONTENT);
            TextView qualityTextLabel = new TextView(context);
            if(pos==0) {
                qualityTextParams.addRule(RelativeLayout.RIGHT_OF, dateLabel.getId());
            } else {
                qualityTextParams.addRule(RelativeLayout.RIGHT_OF, IdValues.REPORT_TITLE_ID + (pos-1) + 150);
            }
            qualityTextParams.setMargins(20,20,0,0);
            qualityTextLabel.setLayoutParams(qualityTextParams);
            qualityTextLabel.setId(IdValues.REPORT_TITLE_ID+pos+150);
            qualityTextLabel.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            qualityTextLabel.setTextAppearance(context, R.style.reportTitleStyle);
            qualityTextLabel.setText((myShared.getString(Keys.QUALITY_NAME+(pos+1) + 150,Keys.QUALITY+Keys.SPACE+(pos+1))));
            reportContentTitleLayout.addView(qualityTextLabel);
        }
        RelativeLayout.LayoutParams shortageTextParams = new RelativeLayout.LayoutParams(200, RelativeLayout.LayoutParams.WRAP_CONTENT);
        shortageTextParams.addRule(RelativeLayout.RIGHT_OF, IdValues.REPORT_TITLE_ID + (pos-1)+150);
        TextView shortageTextLabel = new TextView(context);
        shortageTextParams.setMargins(0,20,0,0);
        shortageTextLabel.setLayoutParams(shortageTextParams);
        shortageTextLabel.setId(IdValues.REPORT_TITLE_ID+pos+150);
        pos++;
        shortageTextLabel.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        shortageTextLabel.setTextAppearance(context, R.style.reportContentStyle);
        shortageTextLabel.setText(activity.getResources().getString(R.string.SHORTAGE));
        reportContentTitleLayout.addView(shortageTextLabel);

        RelativeLayout.LayoutParams totalTextParams = new RelativeLayout.LayoutParams(200, RelativeLayout.LayoutParams.WRAP_CONTENT);
        totalTextParams.addRule(RelativeLayout.RIGHT_OF, IdValues.REPORT_TITLE_ID + (pos-1) + 150);
        TextView totalTextLabel = new TextView(context);
        totalTextParams.setMargins(0,20,0,0);
        totalTextLabel.setLayoutParams(totalTextParams);
        totalTextLabel.setId(IdValues.REPORT_TITLE_ID+pos+150);
        totalTextLabel.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        totalTextLabel.setTextAppearance(context, R.style.reportTitleStyle);
        totalTextLabel.setText(getResources().getString(R.string.TOTAL));
        reportContentTitleLayout.addView(totalTextLabel);
    }

}
