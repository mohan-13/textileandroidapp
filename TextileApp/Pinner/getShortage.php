<?php
/**
 * Created by PhpStorm.
 * User: M.E Muthu
 * Date: 14-08-2018
 * Time: 23:52
 */
include "../database.php";

$ref_id = $_POST['refId'];

$sql = mysqli_query($conn,"select pin.ref_id as ref_id, pin.user_id as user_id, pin.employee_id as emp_id, us.user_name as user_name, pin.date as date,emp.employee_name as emp_name,pin.quality as quality, pin.shortage as shortage,pin.quantity as quantity from pinner pin,employee emp,users us where pin.user_id = us.user_id and pin.employee_id = emp.employee_id and pin.ref_id = $ref_id");

$data = array();

if(mysqli_num_rows($sql)>0) {

    $pos = 0;

    $row = mysqli_fetch_array($sql);

        $data['refId'] = $row['ref_id'];

        $data['userId'] = $row['user_id'];

        $data['userName'] = $row['user_name'];

        $data['empId'] = $row['emp_id'];

        $data['empName'] = $row['emp_name'];

        $data['date'] = $row['date'];

        $data['quality'] = $row['quality'];

        $data['quantity'] = $row['quantity'];

        $data['shortage'] = $row['shortage'];

        $data['original'] = $row['quantity'] + $row['shortage'];

        $pos++;

    $data['size'] = $pos;

}else{
    $data['size'] = 0;
}
echo json_encode($data);

mysqli_close($conn);



?>