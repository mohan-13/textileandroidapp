<?php
/**
 * Created by PhpStorm.
 * User: M.E Muthu
 * Date: 14-08-2018
 * Time: 23:17
 */

include "../database.php";

$from_date = $_POST['fromDate'];

$to_date = $_POST['toDate'];

$qualityCount = $_POST['qualityCount'];

if($_POST['userSpecificReport']==1) {
    $userId = $_POST['userId'];
    $sql = mysqli_query($conn,"select pin.ref_id as ref_id, pin.user_id as user_id, pin.employee_id as emp_id, us.user_name as user_name, pin.date as date,emp.employee_name as emp_name,pin.quality as quality, pin.shortage as shortage,pin.quantity as quantity from pinner pin,employee emp,users us where pin.user_id = us.user_id and pin.employee_id = emp.employee_id and pin.date between '$from_date' and '$to_date' and pin.user_id = $userId");
} else {
    $sql = mysqli_query($conn,"select pin.ref_id as ref_id, pin.user_id as user_id, pin.employee_id as emp_id, us.user_name as user_name, pin.date as date,emp.employee_name as emp_name,pin.quality as quality, pin.shortage as shortage,pin.quantity as quantity from pinner pin,employee emp,users us where pin.user_id = us.user_id and pin.employee_id = emp.employee_id and pin.date between '$from_date' and '$to_date'");
}

$data = array();

if(mysqli_num_rows($sql)>0) {

    $pos = 0;

    while ($row = mysqli_fetch_array($sql)) {

        $data['refId' . $pos] = $row['ref_id'];

        $data['userId' . $pos] = $row['user_id'];

        $data['userName' . $pos] = $row['user_name'];

        $data['empId' . $pos] = $row['emp_id'];

        $data['empName' . $pos] = $row['emp_name'];

        $data['date' . $pos] = $row['date'];

        $data['quality' . $pos] = $row['quality'];

        $data['quantity' . $pos] = $row['quantity'];

        $data['shortage' . $pos] = $row['shortage'];

        for( $ctr = 1 ; $ctr <= $qualityCount ; $ctr++ ) {

            if($row['quality'] == $ctr) {
                $data['quality'.$pos.$ctr] = $row['quantity'];
            }
            else{
                $data['quality'.$pos.$ctr] = 0;
            }
    }

    $pos++;

}
$data['size'] = $pos;
} else {
    $data['size'] = 0;
}
echo json_encode($data);

mysqli_close($conn);

?>