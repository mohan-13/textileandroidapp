<?php
/**
 * Created by PhpStorm.
 * User: M.E Muthu
 * Date: 14-08-2018
 * Time: 22:34
 */

include "../database.php";

$from_date = $_POST['fromDate'];

$to_date = $_POST['toDate'];

$quality_count = $_POST['qualityCount'];

if($_POST['userSpecificReport'] == "1") {
    $userId = $_POST['userId'];
    $sql = mysqli_query($conn,"select super.receipt_no as receipt_no,super.date as date, super.user_id as user_id, super.employee_id as emp_id, us.user_name as user_name, emp.employee_name as emp_name from supervisor super,employee emp,users us where super.user_id = us.user_id and super.employee_id = emp.employee_id and super.date between '$from_date' and '$to_date' and super.user_id = $userId");
} else {
    $sql = mysqli_query($conn,"select super.receipt_no as receipt_no,super.date as date, super.user_id as user_id, super.employee_id as emp_id, us.user_name as user_name, emp.employee_name as emp_name from supervisor super,employee emp,users us where super.user_id = us.user_id and super.employee_id = emp.employee_id and super.date between '$from_date' and '$to_date'");
}

$data = array();

if(mysqli_num_rows($sql)>0){

    $pos = 0;

    while($row=mysqli_fetch_array($sql)){

        $data['receiptNo'.$pos] = $row['receipt_no'];

        $data['userId'.$pos] = $row['user_id'];

        $data['userName'.$pos] = $row['user_name'];

        $data['empId'.$pos] = $row['emp_id'];

        $data['empName'.$pos] = $row['emp_name'];

        $data['date'.$pos] = $row['date'];

        $receipt_no = $row['receipt_no'];

        for( $ctr = 1 ; $ctr <= $quality_count ; $ctr++ ) {

            $query = mysqli_query($conn,"select * from supervisor_quality where receipt_no = $receipt_no and quality_type = $ctr");

            if(mysqli_num_rows($query)>0) {

                $quality_row = mysqli_fetch_array($query);

                $data['quality'.$pos.$ctr] = $quality_row['quantity'];

            }else{
                $data['quality'.$pos.$ctr] = 0;
            }

        }

        $pos++;
    }
    $data['size'] = $pos;

}else{
    $data['size'] = 0;
}

echo json_encode($data);

mysqli_close($conn);

?>