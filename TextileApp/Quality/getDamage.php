<?php
/**
 * Created by PhpStorm.
 * User: M.E Muthu
 * Date: 14-08-2018
 * Time: 23:56
 */

include "../database.php";

$ref_id = $_POST['refId'];

$sql = mysqli_query($conn,"select qual.ref_id as ref_id, qual.user_id as user_id, qual.employee_id as emp_id, us.user_name as user_name, qual.date as date,emp.employee_name as emp_name,qual.quality as quality, qual.damage as damage,qual.quantity as quantity from quality_checker qual,employee emp,users us where qual.user_id = us.user_id and qual.employee_id = emp.employee_id and qual.ref_id = $ref_id");

$data = array();

if(mysqli_num_rows($sql)>0) {

    $pos = 0;

        $row = mysqli_fetch_array($sql);

        $data['refId'] = $row['ref_id'];

        $data['userId'] = $row['user_id'];

        $data['userName'] = $row['user_name'];

        $data['empId'] = $row['emp_id'];

        $data['empName'] = $row['emp_name'];

        $data['date'] = $row['date'];

        $data['quality'] = $row['quality'];

        $data['quantity'] = $row['quantity'];

        $data['damage'] = $row['damage'];

        $data['original'] = $row['quantity'] + $row['damage'];

        $pos++;

    $data['size'] = $pos;

}else{
    $data['size'] = 0;
}
echo json_encode($data);

mysqli_close($conn);


?>