<?php
/**
 * Created by PhpStorm.
 * User: M.E Muthu
 * Date: 11-08-2018
 * Time: 23:35
 */

include "../database.php";

$date=$_POST['date'];

$user_id=$_POST['userId'];

$emp_id=$_POST['empId'];

$quality_count=$_POST['qualityCount'];

$receipt_no=0;

$sql=mysqli_query($conn,"select max(receipt_no) as receipt from supervisor");

if(mysqli_num_rows($sql)>0){

    $row=mysqli_fetch_array($sql);

    $receipt_no=$row['receipt']+1;


}else{
    $receipt_no=1;
}

$sql=mysqli_query($conn,"insert into supervisor(receipt_no,user_id,employee_id,date) values($receipt_no,$user_id,$emp_id,'$date')");

$responseData = array();

if($sql) {

    for ($pos = 0; $pos < $quality_count; $pos++) {
        $quality = $_POST['qualityType' . $pos];

        $quantity = $_POST['quantity' . $pos];
        $quality_break_up_entry = $_POST['qualityBreakUp'.$pos];

        // echo $quality_break_up_entry;

        $sql = mysqli_query($conn, "insert into supervisor_quality(receipt_no, quality_type, quantity) values($receipt_no, $quality, $quantity)");
        $sql = mysqli_query($conn, "update supervisor_quality set quality_break_up_entry = '$quality_break_up_entry' where receipt_no = $receipt_no and quality_type = $quality");
        // echo error_reporting($conn);

    }

    $sql = mysqli_query($conn, "select * from employee where employee_id = $emp_id");

    if (mysqli_num_rows($sql) > 0) {
        $row = mysqli_fetch_array($sql);
        $responseData['result'] = 'success';
        $responseData['empId'] = $row['employee_id'];
        $responseData['empName'] = $row['employee_name'];
        $responseData['receiptNo'] = $receipt_no;
        $responseData['date'] = $date;
    }

}else{
    $responseData['result']='failed';
}
 echo json_encode($responseData);
?>