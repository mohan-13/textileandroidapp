<?php
/**
 * Created by PhpStorm.
 * User: M.E Muthu
 * Date: 14-08-2018
 * Time: 23:58
 */
include "../database.php";

$receipt_no = $_POST['receiptNo'];

$quality_count = $_POST['qualityCount'];

$sql = mysqli_query($conn,"select super.receipt_no as receipt_no,super.date as date, super.user_id as user_id, super.employee_id as emp_id, us.user_name as user_name, emp.employee_name as emp_name from supervisor super,employee emp,users us where super.user_id = us.user_id and super.employee_id = emp.employee_id and super.receipt_no = $receipt_no");

$data = array();

if(mysqli_num_rows($sql)>0){

    $pos = 0;

    if($row=mysqli_fetch_array($sql)){

        $data['receiptNo'] = $row['receipt_no'];

        $data['userId'] = $row['user_id'];

        $data['userName'] = $row['user_name'];

        $data['empId'] = $row['emp_id'];

        $data['empName'] = $row['emp_name'];

        $data['date'] = $row['date'];

        $receipt_no = $row['receipt_no'];

        for( $ctr = 1 ; $ctr <= $quality_count ; $ctr++ ){

            $query = mysqli_query($conn,"select * from supervisor_quality where receipt_no = $receipt_no and quality_type = $ctr");

            if(mysqli_num_rows($query)>0){

                $quality_row = mysqli_fetch_array($query);

                if($quality_row['quality_break_up_entry'] == null) {
                    $data['quality'.$ctr] = $quality_row['quantity'];
                }
                else {
                    $data['quality'.$ctr] = $quality_row['quality_break_up_entry'];
                }

            }else{
                $data['quality'.$ctr] = 0;
            }

        }

        $pos++;
    }
    $data['size'] = 1;

}else{
    $data['size'] = 0;
}

echo json_encode($data);

mysqli_close($conn);



?>